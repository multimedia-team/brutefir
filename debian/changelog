brutefir (1.0o-2) unstable; urgency=medium

  * Team upload

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Set Vcs-* to salsa.debian.org
  * d/changelog: Remove trailing whitespaces

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat

  [ Sebastian Ramacher ]
  * debian/rules: Drop unused ARCH
  * debian/: Bump debhelper compat to 12
  * debian/control: Bump Standards-Version

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 08 Dec 2019 20:47:08 +0100

brutefir (1.0o-1) unstable; urgency=medium

  * Imported Upstream version 1.0o
  * Drop patches applied upstream.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Tue, 09 Aug 2016 20:22:27 +0200

brutefir (1.0n-1) unstable; urgency=medium

  * Improved description of patch.
  * Patches forwarded.
  * Update copyright file.
  * Improve .gitignore file.
  * Imported Upstream version 1.0n
    (Closes: #715739) (Closes: #715866) (Closes: #715903)
    (Closes: #716035) (Closes: #716131)
  * Fix some hardening.
  * Don't sign tags.
  * Fix more spelling errors.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Tue, 09 Aug 2016 13:14:05 +0200

brutefir (1.0m-1) unstable; urgency=medium

  * Team Upload.

  [ Jaromír Mikeš ]
  * Imported Upstream version 1.0m
  * Set dh/compat 9.
  * Bump Standards.
  * Add myself as uploader.
  * Patches refreshed.
  * Little fix in description.
  * Added patch partially fix hardening.
  * Added patch fix FTBFS on kfreebsd-i386 (Closes: #728139).

  [ Felipe Sateler ]
  * Use gcc instead of ld directly to link.
  * Pass LDFLAGS to brutefir link.
  * Change _init functions to use constructor attributes instead
  * Enable parallel builds

 -- Felipe Sateler <fsateler@debian.org>  Tue, 25 Feb 2014 22:42:05 -0300

brutefir (1.0l-1) unstable; urgency=low

  * Team upload.
  * New upstream release:
    - Made the code compile well on x86-64.
    - Replaced legacy assembler code with new SSE/SSE2 C code,
      3DNow support dropped.
    - Fixed filter indexing bug in the 'cffa' CLI command.
    - S24_LE now maps to Alsa SND_PCM_FORMAT_S24_3LE and S24_4LE to
    - SND_PCM_FORMAT_S24_LE (same for BE of course). Also added possibility
      to use Alsa syntax "S24_3LE" in config file (means same as the old
      "S24_LE").
    - Refreshed JACK I/O module to make up to date with current API
      versions.
  * Refresh patches.
  * Replace negated list of architectures with linux-any (Closes: #634790).
  * Use canonical form for VCS urls.
  * Remove myself from the Uploaders field.
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Tue, 15 Oct 2013 14:00:42 +0100

brutefir (1.0k-2) unstable; urgency=low

  * Upload to unstable.

 -- Alessio Treglia <alessio@debian.org>  Tue, 08 Feb 2011 22:28:43 +0100

brutefir (1.0k-1) experimental; urgency=low

  * New upstream release.
  * Switch to format 3.0 (quilt).
  * Switch to debhelper 7.
  * Change fftw3-dev to libfftw3-dev (Closes: #474591); thanks to
    James Westby for reporting this.
  * Update watch file.
  * debian/control:
    - Add .gitignore file.
    - Add ${misc:Depends} to the Depends line.
    - Add Homepage field.
    - Bump Standards.
  * Convert patches to the quilt format.
  * Remove direct changes to the upstream sources. Create new patch to
    apply with quilt.
  * Add patch to fix spelling errors.
  * Move manpage into the debian directory.
  * Rename 10_Makefile.dpatch -> 10-makefile.patch, refresh.
  * Remove old 00list* files.
  * Add local-options file.
  * Update copyright information.
  * Fix debian menu file.
  * Fix doc-base file.
  * Fix copyright-refers-to-symlink-license warning.
  * Rename bruterfir.orig to brutefir.real.
  * Remove manpage in SGML format.
  * Install configuration files as package examples.
  * Add configuration file for git-buildpackage.

 -- Alessio Treglia <alessio@debian.org>  Mon, 20 Sep 2010 15:22:21 +0200

brutefir (1.0f-2) unstable; urgency=low

  * debian/patches/10_Makefile.dpatch: Add patches from Cyril Brulebois
    to fix FTBFS on non-linux architectures (which don't provide ALSA
    support). Closes: #414298
  * debian/control:
    - Limit libasound2-dev to linux-archs only.
    - Add Vcs fields.
    - Change Maintainer and update Uploaders list (Closes: #546949).
  * debian/compat: Bump compatibility level up to 5.

 -- Alessio Treglia <alessio@debian.org>  Mon, 20 Sep 2010 13:01:14 +0200

brutefir (1.0f-1) unstable; urgency=low

  * New upstream release
    - Fixes invalid lvalues in assignments (Closes: #320270)
  * Rebuild for the jack transition (Closes: #317185)
  * Added wrapper script to create an empty config file (Closes: #318202)
  * Dropped the amd64 gcc-4.0 patch, fixed upstream

 -- Free Ekanayaka <free@agnula.org>  Sun,  2 Oct 2005 18:12:04 +0100

brutefir (1.0c-1) unstable; urgency=low

  * New upstream release
  * Using dpatch
  * Added dpatch for the Makefile
  * Bug fix: "brutefir: FTBFS (amd64/gcc-4.0): invalid lvalue in
    assignment", thanks to Andreas Jochens (Closes: #284751).
  * Bug fix: "brutefir: ftbfs [sparc] Requires v9|v9a|v9b", thanks to
    Blars Blarson (Closes: #269715).

 -- Free Ekanayaka <free@agnula.org>  Tue, 22 Mar 2005 13:00:44 +0100

brutefir (1.0-1) unstable; urgency=low

  * New upstream release

 -- Free Ekanayaka <free@agnula.org>  Fri, 11 Jun 2004 21:52:06 +0200

brutefir (0.99n-2) unstable; urgency=low

  * debian/control: removed double spaces in the Desctiption field
    (closes #241576)
  * Removed menu file, as BruteFIR is supposed to be invoked from the
    command line.

 -- Free Ekanayaka <free@agnula.org>  Wed, 14 Apr 2004 12:59:47 +0200

brutefir (0.99n-1) unstable; urgency=low

  * New upstream release
  * GGeiger: Fixed endian detection on non-sparc or intel machines
  * GGeiger: Fixed compilation on non-sparc or intel (intel assembler)
  * First upload to Debian (closes: #187276)

 -- Free Ekanayaka <free@agnula.org>  Sat, 28 Feb 2004 19:58:31 +0100

brutefir (0.99l-0) unstable; urgency=low

  * Added template man page
  * New upstream release
  * adjusted description formatting

 -- Free Ekanayaka <free@agnula.org>  Fri, 21 Nov 2003 16:11:28 +0600

brutefir (0.99k-1) unstable; urgency=low

  * Initial Release.

 -- Free Ekanayaka <free@agnula.org>  Thu, 18 Sep 2003 23:06:33 +0200
